module Fuel
where

import System.IO
import Control.Monad
import Data.Typeable

main = do
  inHandler <- openFile "input.txt" ReadMode
  contents <- hGetContents inHandler
  print $ sum $ map (fuel.read) $ words contents
  hClose inHandler

fuel :: Int -> Int
fuel x = (div x 3) - 2
