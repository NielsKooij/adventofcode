package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class SeatTest {

    @Test
    fun examplesTest() {
        assertEquals(357, Seat.calculateSeatId("FBFBBFFRLR"))
        assertEquals(567, Seat.calculateSeatId("BFFFBBFRRR"))
    }

}