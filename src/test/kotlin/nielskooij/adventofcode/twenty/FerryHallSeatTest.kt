package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class FerryHallSeatTest {

    @Test
    fun examplePart1() {
        assertEquals(37, FerryHallSeat.calculateOccupiedSeats(input1()))
    }

    @Test
    fun examplePart2() {
        assertEquals(26, FerryHallSeat.calculateOccupiedSeats2(input1()))
    }

    @Test
    fun visibleSeatsTest() {
        val matrix = FerryHallSeat.parseMatrix(input1())

        assertEquals(Pair(1, 3), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair(-1, -1)))
        assertEquals(Pair(1, 4), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair(-1,  0)))
        assertEquals(Pair(1, 5), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair(-1,  1)))
        assertEquals(Pair(2, 2), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair( 0, -1)))
        assertEquals(Pair(2, 7), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair( 0,  1)))
        assertEquals(Pair(3, 3), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair( 1, -1)))
        assertEquals(Pair(5, 4), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair( 1,  0)))
        assertEquals(Pair(3, 5), FerryHallSeat.findFirst(matrix, Pair(2, 4), Pair( 1,  1)))
    }

    fun input1() =
        """
        L.LL.LL.LL
        LLLLLLL.LL
        L.L.L..L..
        LLLL.LL.LL
        L.LL.LL.LL
        L.LLLLL.LL
        ..L.L.....
        LLLLLLLLLL
        L.LLLLLL.L
        L.LLLLL.LL
        """.trimIndent().split("\n")

}