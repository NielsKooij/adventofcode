package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class BagTest {

    @Test
    fun testExample() {
        val input = """
            shiny gold bags contain 1 faded blue bag, 1 dark olive bag, 2 dotted black bags, 2 vibrant plum bags. 
            faded blue bags contain no other bags.
            dotted black bags contain no other bags.
            vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
            dark olive bags contain 3 faded blue bags, 4 dotted black bags.
        """.trimIndent().split("\n")

        val result = Bags.calculateInsideShinyGold(input)
        assertEquals(32, result)
    }

    @Test
    fun testExampl2() {
        val input = """
            shiny gold bags contain 2 dark red bags.
            dark red bags contain 2 dark orange bags.
            dark orange bags contain 2 dark yellow bags.
            dark yellow bags contain 2 dark green bags.
            dark green bags contain 2 dark blue bags.
            dark blue bags contain 2 dark violet bags.
            dark violet bags contain no other bags.
        """.trimIndent().split("\n")

        val result = Bags.calculateInsideShinyGold(input)
        assertEquals(126, result)
    }
}