package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class BitProgramTest {

    @Test
    fun examplePart1() {
        assertEquals(165, BitProgram.runProgram(input()))
    }

    @Test
    fun examplePart2() {
        assertEquals(208, BitProgram.runFloatingProgram(input2()))
    }

    fun input() =
        """
            mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            mem[8] = 11
            mem[7] = 101
            mem[8] = 0
        """.trimIndent().split("\n")

    fun input2() =
        """
            mask = 000000000000000000000000000000X1001X
            mem[42] = 100
            mask = 00000000000000000000000000000000X0XX
            mem[26] = 1
        """.trimIndent().split("\n")

}