package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class BusTest {


    @Test
    fun examplePart2() {
        assertEquals("1068781", Bus.firstSubsequentDeparture(input()))
    }

    fun input(): List<String> =
        """
          939
          7,13,x,x,59,x,31,19
        """.trimIndent().split("\n")

}