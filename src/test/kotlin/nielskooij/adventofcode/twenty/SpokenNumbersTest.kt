package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class SpokenNumbersTest
{

    @Test
    fun examples() {
        assertEquals(436, SpokenNumbers.nthSpokenNumber("0,3,6", 2020))
        assertEquals(1, SpokenNumbers.nthSpokenNumber("1,3,2", 2020))
        assertEquals(10, SpokenNumbers.nthSpokenNumber("2,1,3", 2020))
        assertEquals(27, SpokenNumbers.nthSpokenNumber("1,2,3", 2020))
        assertEquals(78, SpokenNumbers.nthSpokenNumber("2,3,1", 2020))
        assertEquals(438, SpokenNumbers.nthSpokenNumber("3,2,1", 2020))
        assertEquals(1836, SpokenNumbers.nthSpokenNumber("3,1,2", 2020))
    }

}
