package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test
import java.math.BigInteger

class OperationOrderTest
{

    @Test
    fun examplesBest() {
        for(i in inputs().indices) {
            val test = input(i)
            assertEquals("$i", BigInteger.valueOf(test.first), OperationOrder.eval(test.second.replace(" ", "")))
        }
    }

    fun input(index: Int): Pair<Long, String> =
        inputs()[index]

    fun inputs() =
        listOf(
            Pair(71L, "1 + 2 * 3 + 4 * 5 + 6"),
            Pair(51L, "1 + (2 * 3) + (4 * (5 + 6))"),
            Pair(18162144L, "(6 + 7 * 4) * 4 * (7 + 7 * 9) * (4 + 6 * 3 + 3) * 3 * 7"),
            Pair(26L, "2 * 3 + (4 * 5)"),
            Pair(437L, "5 + (8 * 3 + 9 + 3 * 4 * 3)"),
            Pair(12240L, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
            Pair(13632L, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"),
            Pair(28L, "4 * 7"),
            Pair(7378560L, "7 * ((7 * 2) * (3 * 4) + 8 + (8 * 6 + 5 + 2) * 8 * (6 * 9 * 3)) * 3 + (8 * (4 * 3 + 8 * 9 * 6 + 3) * 7 * 6 * 3)")
        )

    @Test
    fun examplesPrecedence() {
        for(i in inputsPrecedence().indices) {
            val test = inputPrecedence(i)
            assertEquals("$i", BigInteger.valueOf(test.first), OperationOrder.evalPrecedence(test.second.replace(" ", "")))
        }
    }

    fun inputPrecedence(index: Int): Pair<Long, String> =
        inputsPrecedence()[index]

    fun inputsPrecedence() =
        listOf(
            Pair(51L, "1 + (2 * 3) + (4 * (5 + 6))"),
            Pair(46L, "2 * 3 + (4 * 5)"),
            Pair(1445L, "5 + (8 * 3 + 9 + 3 * 4 * 3)"),
            Pair(669060L, "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"),
            Pair(23340L, "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
        )


}
