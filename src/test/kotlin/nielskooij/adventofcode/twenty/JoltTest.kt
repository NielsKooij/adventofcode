package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class JoltTest {

    @Test
    fun testExample() {
        assertEquals(22 * 10, Jolts.joltDifference(input1()))
    }

    @Test
    fun example2() {
        assertEquals(7 * 5, Jolts.joltDifference(input2()))
    }

    @Test
    fun part2ExampleTest() {
        assertEquals(19208, Jolts.arrange(input1()))
    }

    fun input1() =
        """
            0
            28
            33
            18
            42
            31
            14
            46
            20
            48
            47
            24
            23
            49
            45
            19
            38
            39
            11
            1
            32
            25
            35
            8
            17
            7
            9
            4
            2
            34
            10
            3
        """.trimIndent().split("\n").map { it.toInt() }

    fun input2() =
        """
            0
            16
            10
            15
            5
            1
            11
            7
            19
            6
            12
            4
        """.trimIndent().split("\n").map { it.toInt() }
}