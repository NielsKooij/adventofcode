package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class ConwayCubeTest
{

    @Test
    fun testExample() {
        assertEquals(112, ConwayCube.calculateActive(input(), 6))
    }

    fun input() =
        """
            .#.
            ..#
            ###
        """.trimIndent().split("\n")

}
