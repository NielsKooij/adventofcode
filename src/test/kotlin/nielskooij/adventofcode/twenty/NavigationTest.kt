package nielskooij.adventofcode.twenty

import junit.framework.Assert.assertEquals
import org.junit.Test

class NavigationTest {

    @Test
    fun examplePart1() {
        assertEquals(25, Navigation.navigate(input1()))
    }

    @Test
    fun examplePart2() {
        assertEquals(286, Navigation.navigateWaypoint(input1()))
    }

    fun input1(): List<String> =
        """
            F10
            N3
            F7
            R90
            F11
        """.trimIndent().split("\n")

}