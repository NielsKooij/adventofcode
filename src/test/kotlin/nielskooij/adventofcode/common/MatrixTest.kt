package nielskooij.adventofcode.common

import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MatrixTest {

    private lateinit var matrix: Matrix<Int>

    @Before
    fun setup() {
        val data = arrayOf(
            arrayOf(0, 1, 2),
            arrayOf(3, 4, 5),
            arrayOf(6, 7, 8),
            arrayOf(9, 10, 11)
        ).flatMap { it.toList() }

        matrix = Matrix(data, 4, 3)
    }

    @Test
    fun getAdjecentTest() {
        assertEquals("0,0", listOf(1, 3, 4), matrix.getAdjacentCells(Pair(0, 0)).sorted())
        assertEquals("0,1", listOf(0, 2, 3, 4, 5), matrix.getAdjacentCells(Pair(0, 1)).sorted())
        assertEquals("0,2", listOf(1, 4, 5), matrix.getAdjacentCells(Pair(0, 2)).sorted())
        assertEquals("1,0", listOf(0, 1, 4, 6, 7), matrix.getAdjacentCells(Pair(1, 0)).sorted())
        assertEquals("1,1", listOf(0, 1, 2, 3, 5, 6, 7, 8), matrix.getAdjacentCells(Pair(1, 1)).sorted())
        assertEquals("1,2", listOf(1, 2, 4, 7, 8), matrix.getAdjacentCells(Pair(1, 2)).sorted())
        assertEquals("2,0", listOf(3, 4, 7, 9, 10), matrix.getAdjacentCells(Pair(2, 0)).sorted())
        assertEquals("2,1", listOf(3, 4, 5, 6, 8, 9, 10, 11), matrix.getAdjacentCells(Pair(2, 1)).sorted())
        assertEquals("2,2", listOf(4, 5, 7, 10, 11), matrix.getAdjacentCells(Pair(2, 2)).sorted())
        assertEquals("3,0", listOf(6, 7, 10), matrix.getAdjacentCells(Pair(3, 0)).sorted())
        assertEquals("3,1", listOf(6, 7, 8, 9, 11), matrix.getAdjacentCells(Pair(3, 1)).sorted())
        assertEquals("3,2", listOf(7, 8, 10), matrix.getAdjacentCells(Pair(3, 2)).sorted())
    }

    @Test
    fun dimToIndexTest() {
        var count = 0
        for(m in 0 until 4) {
            for (n in 0 until 3) {
                assertEquals("($m, $n)", count, matrix.get(Pair(m, n)))
                count++
            }
        }
    }

    @Test
    fun indexToDimTest() {
        var count = 0
        for(m in 0 until 4) {
            for (n in 0 until 3) {
                assertEquals("($count)", Pair(m, n), matrix.indexToDim(count))
                count++
            }
        }
    }

}