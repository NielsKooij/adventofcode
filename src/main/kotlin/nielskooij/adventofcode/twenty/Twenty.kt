package nielskooij.adventofcode.twenty

import nielskooij.adventofcode.common.ResourceHelper

class Twenty

fun main()
{
    println("Day 1.1: ${Sum.sum2020(ResourceHelper.getResourceAsIntList("/2020/Day1.txt"))}")
//    println("Day 1.2: ${Sum.sum2020x(ResourceHelper.getResourceAsIntList("/2020/Day1.txt"))}")

    println("Day 2.1: ${Password.countValid(ResourceHelper.getResource("/2020/Day2.txt"), Password::maxOcurrence)}")
    println("Day 2.2: ${Password.countValid(ResourceHelper.getResource("/2020/Day2.txt"), Password::xorIndex)}")

    println("Day 3.1: ${Airport.countTrees(ResourceHelper.getResource("/2020/Day3.txt"))}")
    println("Day 3.2: ${Airport.countTrees2(ResourceHelper.getResource("/2020/Day3.txt"))}")

    println("Day 4.1: ${Passport.countValid(ResourceHelper.getResourceAsString("/2020/Day4.txt"), Passport::containsRequiredFields)}")
    println("Day 4.2: ${Passport.countValid(ResourceHelper.getResourceAsString("/2020/Day4.txt"), Passport::isValidPassport)}")

    println("Day 5.1: ${Seat.calculateHighestSeatId(ResourceHelper.getResource("/2020/Day5.txt"))}")
    println("Day 5.2: ${Seat.findMissingSeat(ResourceHelper.getResource("/2020/Day5.txt"))}")

    println("Day 6.1: ${Questions.countAllAnswers(ResourceHelper.getResourceAsString("/2020/Day6.txt"))}")
    println("Day 6.2: ${Questions.countIntersectingAnswers(ResourceHelper.getResourceAsString("/2020/Day6.txt"))}")

    println("Day 7.1: ${Bags.calculateBags(ResourceHelper.getResource("/2020/Day7.txt"))}")
    println("Day 7.2: ${Bags.calculateInsideShinyGold(ResourceHelper.getResource("/2020/Day7.txt"))}")

    println("Day 8.1: ${Program.findStateAtLoop(ResourceHelper.getResource("/2020/Day8.txt"))}")
    println("Day 8.2: ${Program.repair(ResourceHelper.getResource("/2020/Day8.txt"))}")

    println("Day 9.1: ${XMAS.crackCipher(ResourceHelper.getResource("/2020/Day9.txt").map { it.toLong() })}")
    println("Day 9.2: ${XMAS.crackCypher2(ResourceHelper.getResource("/2020/Day9.txt").map { it.toLong() })}")

    println("Day 10.1: ${Jolts.joltDifference(ResourceHelper.getResource("/2020/Day10.txt").map { it.toInt() })}")
    println("Day 10.2: ${Jolts.arrange(ResourceHelper.getResource("/2020/Day10.txt").map { it.toInt() })}")

    println("Day 11.1: ${FerryHallSeat.calculateOccupiedSeats(ResourceHelper.getResource("/2020/Day11.txt"))}")
    println("Day 11.2: ${FerryHallSeat.calculateOccupiedSeats2(ResourceHelper.getResource("/2020/Day11.txt"))}")

    println("Day 12.1: ${Navigation.navigate(ResourceHelper.getResource("/2020/Day12.txt"))}")
    println("Day 12.2: ${Navigation.navigateWaypoint(ResourceHelper.getResource("/2020/Day12.txt"))}")

    println("Day 13.1: ${Bus.firstDeparture(ResourceHelper.getResource("/2020/Day13.txt"))}")
    println("Day 13.2: ${Bus.firstSubsequentDeparture(ResourceHelper.getResource("/2020/Day13.txt"))}")

    println("Day 14.1: ${BitProgram.runProgram(ResourceHelper.getResource("/2020/Day14.txt"))}")
    println("Day 14.2: ${BitProgram.runFloatingProgram(ResourceHelper.getResource("/2020/Day14.txt"))}")

    println("Day 15.1: ${SpokenNumbers.nthSpokenNumber(ResourceHelper.getResourceAsString("/2020/Day15.txt").replace("\n", ""), 2020)}")
//    println("Day 15.2: ${SpokenNumbers.nthSpokenNumber(ResourceHelper.getResourceAsString("/2020/Day15.txt").replace("\n", ""), 30000000)}")

    println("Day 16.1: ${TrainTicket.ticketScanningErrorRate(ResourceHelper.getResourceAsString("/2020/Day16.txt"))}")
    println("Day 16.2: ${TrainTicket.departureValues(ResourceHelper.getResourceAsString("/2020/Day16.txt"))}")

    println("Day 17.1: ${ConwayCube.calculateActive(ResourceHelper.getResource("/2020/Day17.txt"), 6)}")
    println("Day 17.2: ${ConwayCube.calculateActive4D(ResourceHelper.getResource("/2020/Day17.txt"), 6)}")

    println("Day 18.1: ${OperationOrder.evalAll(ResourceHelper.getResource("/2020/Day18.txt"))}")
    println("Day 18.2: ${OperationOrder.evalAllPrecedence(ResourceHelper.getResource("/2020/Day18.txt"))}")

    println("Day 19.1: ${MonsterMessages.nrOfMatches(ResourceHelper.getResourceAsString("/2020/Day19.txt"))}")
    println("Day 19.2: ${MonsterMessages.nrOfMatchesLoop(ResourceHelper.getResourceAsString("/2020/Day19.txt"))}")
}
