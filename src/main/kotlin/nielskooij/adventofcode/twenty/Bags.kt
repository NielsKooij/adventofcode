package nielskooij.adventofcode.twenty

import java.lang.IllegalStateException

object Bags {

    fun calculateBags(input: List<String>): Int {
        val cleanInput = cleanInput(input)
        return cleanInput.keys.fold(-1) { acc, i -> if (canReach(i, "shiny gold", cleanInput)) acc + 1 else acc }
    }

    fun canReach(from: String, to: String, mappings: Map<String, List<Pair<Int, String>>>): Boolean =
        from == to ||
                mappings[from]?.let {
                    it.map { pair -> canReach(pair.second, to, mappings) }.any { bool -> bool }
                } ?: throw IllegalStateException("No mapping found for '$from'")

    fun calculateInsideShinyGold(input: List<String>): Long =
        countInnerBags("shiny gold", cleanInput(input)) - 1


    fun countInnerBags(bag: String, mappings: Map<String, List<Pair<Int, String>>>): Long =
        mappings[bag]?.let {
            if(it.isEmpty()) {
                1
            } else {
                it.map { (count, bag) -> count * countInnerBags(bag, mappings) }.sum() + 1
            }
        } ?: throw IllegalStateException("No mapping found for '$bag'")

    private fun cleanInput(input: List<String>): Map<String, List<Pair<Int, String>>> =
        input.map {
            val cleanLine = it.replace(" bags", "")
                .replace(" bag", "")
                .replace(".", "")

            val parts = cleanLine.split("contain")

            if (parts[1].trim() == "no other") {
                parts[0].trim() to listOf()
            } else {
                val contain: List<Pair<Int, String>> = parts[1].split(",").map { pair -> pair.trim() }
                    .map { bagPair ->
                        val indexOfSpace = bagPair.indexOf(" ")
                        Pair(bagPair.substring(0, indexOfSpace).trim().toInt(), bagPair.substring(indexOfSpace).trim())
                    }

                parts[0].trim() to contain
            }
        }.toMap()
}