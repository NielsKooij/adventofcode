package nielskooij.adventofcode.twenty

object TrainTicket
{

    fun ticketScanningErrorRate(input: String): Int =
        parseInput(input).let { ticketScanningErrorRate(it.first, it.second) }

    private fun ticketScanningErrorRate(rules: Rules, tickets: Tickets): Int =
        tickets.map { ticket ->
            ticket.filter { tNr -> rules.none { r -> r.isValid(tNr) } }.sum()
        }.sum()

    fun departureValues(input: String): Long {
        val parsedInput = parseInput(input)
        val validTickets = parsedInput.second.filter { t ->
            t.all { tNr -> parsedInput.first.any { r -> r.isValid(tNr) } }
        }
        val rules = parsedInput.first
        val myTicket = parsedInput.third

        val possibleWithRule = rules.map { rule ->
            rule to myTicket.indices.toList().filter { i ->
                validTickets.all { ticket ->
                    rule.isValid(ticket[i])
                }
            }.toSet()
        }.toMap()

        val ticketIndices = mutableListOf<Int>()
        var currentMap = possibleWithRule
        while (currentMap.any { it.value.size > 1 }) {
            val nextKnown = currentMap.filter { it.value.size == 1 }.entries.single()

            currentMap = currentMap.map { (k, v) -> k to v.subtract(nextKnown.value)  }.toMap()
            if(nextKnown.key.name.contains("departure")) {
                ticketIndices.add(nextKnown.value.single())
            }
        }


        return myTicket.foldIndexed(1L) { index, acc, i ->  if(index in ticketIndices) acc * i.toLong() else acc }
    }

    private fun parseInput(input: String): Triple<Rules, Tickets, Ticket> {
        val parts = input.split("\n\n").map { it.split("\n") }

        val rules = parts[0].map { rule ->
            val ruleParts = rule.split(": ")
            val ranges = ruleParts[1].replace(" or", "").split(" ").map { range ->
                val rangeParts = range.split("-")
                (rangeParts[0].toInt())..(rangeParts[1].toInt())
            }
            Rule(ruleParts[0], ranges)
        }

        val tickets = parts[2].mapNotNull { ticket ->
            if(ticket.isEmpty()) null else parseTicket(ticket)
        }

        val myTicket = parseTicket(parts[1].single())

        return Triple(rules, tickets, myTicket)
    }

    private fun parseTicket(ticket: String): Ticket =
        ticket.split(",").map { it.toInt() }

}

class Rule(val name: String, val ranges: List<IntRange>) {

    fun isValid(number: Int): Boolean =
        ranges.any { number in it }
}

typealias Ticket = List<Int>

typealias Rules = List<Rule>
typealias Tickets = List<Ticket>
