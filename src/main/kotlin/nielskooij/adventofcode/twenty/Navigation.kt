package nielskooij.adventofcode.twenty

import nielskooij.adventofcode.util.add
import nielskooij.adventofcode.util.scale
import java.lang.IllegalStateException
import kotlin.math.abs

object Navigation {

    fun navigate(input: List<String>): Int {
        val actions = parseInput(input)

        val end = actions.fold(Turtle(Direction.EAST, Pair(0, 0))) { turtle, action ->
            turtle.move(action.first, action.second)
        }
        return abs(end.position.first) + abs(end.position.second)
    }

    fun navigateWaypoint(input: List<String>): Int {
        val actions = parseInput(input)

        val end = actions.fold(Ship(Pair(0, 0), Pair(-1, 10))) { ship, action ->
            ship.move(action.first, action.second)
        }
        return abs(end.position.first) + abs(end.position.second)
    }

    private fun parseInput(input: List<String>): List<Pair<String, Int>> =
        input.map {
            Pair(it.substring(0, 1), it.substring(1).toInt())
        }

    enum class Direction(val degrees: Int, val vectorDirection: Pair<Int, Int>) {
        NORTH(0, Pair(-1, 0)),
        WEST(90, Pair(0, -1)),
        SOUTH(180, Pair(1, 0)),
        EAST(270, Pair(0, 1));

        fun addDegrees(degrees: Int): Direction =
            fromDegrees((this.degrees + degrees + 360) % 360)!!

        fun move(start: Pair<Int, Int>, steps: Int): Pair<Int, Int> =
            start.add(vectorDirection.scale(steps))

        companion object {
            fun fromDegrees(degrees: Int): Direction? =
                when (degrees) {
                    0 -> NORTH
                    90 -> WEST
                    180 -> SOUTH
                    270 -> EAST
                    else -> null
                }

            fun fromString(string: String): Direction? =
                when (string) {
                    "N" -> NORTH
                    "W" -> WEST
                    "S" -> SOUTH
                    "E" -> EAST
                    else -> null
                }
        }
    }

    class Turtle(val direction: Direction, val position: Pair<Int, Int>) {

        fun move(action: String, steps: Int): Turtle =
            Direction.fromString(action)?.let {
                Turtle(direction, it.move(position, steps))
            } ?:let {
                when(action) {
                    "F" -> Turtle(direction, direction.move(position, steps))
                    "L" -> Turtle(direction.addDegrees(steps), position)
                    "R" -> Turtle(direction.addDegrees(-1 * steps), position)
                    else -> throw IllegalStateException("Unknown action $action")
                }
            }
    }

    class Ship(val position: Pair<Int, Int>, val waypoint: Pair<Int, Int>) {

        fun move(action: String, steps: Int): Ship =
            Direction.fromString(action)?.let {
                Ship(position, waypoint.add(it.vectorDirection.scale(steps)))
            } ?:let {
                when(action) {
                    "F" -> Ship(position.add(waypoint.scale(steps)), waypoint)
                    "L" -> Ship(position, rotateLeft(waypoint, steps / 90))
                    "R" -> Ship(position, rotateRight(waypoint, steps / 90))
                    else -> throw IllegalStateException("Unknown action $action")
                }
            }

        private fun rotateLeft(pos: Pair<Int, Int>, times: Int): Pair<Int, Int> =
            (0 until times).toList().fold(pos) { acc, _ ->
                Pair(acc.second * -1, acc.first)
            }

        private fun rotateRight(pos: Pair<Int, Int>, times: Int): Pair<Int, Int> =
            (0 until times).toList().fold(pos) { acc, _ ->
                Pair(acc.second, acc.first * -1)
            }

    }
}