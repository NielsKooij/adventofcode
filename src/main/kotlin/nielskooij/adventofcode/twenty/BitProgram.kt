package nielskooij.adventofcode.twenty

import java.lang.IllegalArgumentException
import kotlin.math.pow

object BitProgram {

    fun runProgram(input: List<String>): Long {
        val memory = mutableMapOf<Int, Long>().withDefault { 0 }

        var mask = listOf<Bit?>()
        input.forEach { instruction ->
            val parts = instruction.split(" = ")
            val action = parts[0].replace("[", " ").replace("]", "").split(" ")

            if (action[0] == "mask") {
                mask = createBitmask(parts[1])
            } else if (action[0] == "mem") {
                val value = parts[1].toLong().toBitList(36).applyMask(mask).toLong()
                memory[action[1].toInt()] = value
            }
        }

        return memory.values.sum()
    }

    fun runFloatingProgram(input: List<String>): Long {
        val memory = mutableMapOf<Long, Long>().withDefault { 0 }

        var mask = listOf<Bit?>()
        input.forEach { instruction ->
            val parts = instruction.split(" = ")
            val action = parts[0].replace("[", " ").replace("]", "").split(" ")

            if (action[0] == "mask") {
                mask = createBitmask(parts[1])
            } else if (action[0] == "mem") {
                val value = parts[1].toLong()

                val addresses = action[1].toLong().toBitList(36).applyFloatingMask(mask).map { it.toLong() }
                addresses.forEach {
                    memory[it] = value
                }
            }
        }

        return memory.values.sum()
    }

    private fun createBitmask(mask: String): Bitmask =
        mask.toCharArray().map { c ->
            when(c) {
                '0' -> Bit.O
                '1' -> Bit.I
                else -> null
            }
        }
}

typealias Bitmask = List<Bit?>

enum class Bit(val value: Long) { I(1), O(0) }

fun List<Bit>.applyMask(mask: Bitmask): List<Bit> =
    if (size == mask.size) this.zip(mask).map { if(it.second != null) it.second!! else it.first }
    else throw IllegalArgumentException("The amount of bits ($size) must be the same as the length of the mask (${mask.size}).")

fun List<Bit>.applyFloatingMask(mask: Bitmask): List<List<Bit>> =
    if (size != mask.size) throw IllegalArgumentException("The amount of bits ($size) must be the same as the length of the mask (${mask.size}).")
    else {
        this.zip(mask).fold(listOf(mutableListOf())) { acc, pair ->
            when (pair.second) {
                Bit.O -> acc.map { it.apply { add(pair.first) } }
                Bit.I -> acc.map { it.apply { add(pair.second!!) } }
                else -> {
                    val iMap = acc.map { list -> mutableListOf<Bit>().apply {
                        addAll(list)
                        add(Bit.I) } }
                    val oMap = acc.map { list -> mutableListOf<Bit>().apply {
                        addAll(list)
                        add(Bit.O) } }
                    iMap.plus(oMap)
                }
            }
        }
    }

fun List<Bit>.toLong(): Long =
    this.reversed().foldIndexed(0L) { i, acc, bit -> acc + (2.0.pow(i)).toLong() * bit.value }

fun Long.toBitList(size: Int): List<Bit> {
    val bits = mutableListOf<Bit>()

    var bit = (2.0.pow(size - 1)).toLong()
    var value = this
    while (bit >= 1L) {
        if(value / bit == 1L) bits.add(Bit.I)
        else bits.add(Bit.O)

        value %= bit
        bit /= 2
    }

    return bits
}

