package nielskooij.adventofcode.twenty

object MonsterMessages
{

    fun nrOfMatches(input: String): Int
    {
        val parts = input.split("\n\n")

        val regex = buildRegex(parts[0].split("\n"))
        val lines = parts[1].split("\n")

        return lines.count { regex.matches(it) }
    }

    fun buildRegex(rules: List<String>): Regex
    {
        val ruleMap = rules.map { r ->
            val parts = r.split(":")
            parts[0].toInt() to parts[1].trim()
        }.toMap()

        return buildRegex(ruleMap[0]!!.trim(), ruleMap).toRegex()
    }

    fun nrOfMatchesLoop(input: String): Int
    {
        val parts = input.split("\n\n")

        val rules = parts[0].split("\n")
        val lines = parts[1].split("\n")

        return nrMatchesLoops(rules, lines)
    }

    private fun nrMatchesLoops(rules: List<String>, lines: List<String>): Int
    {
        val ruleMap = rules.map { r ->
            val parts = r.split(":")
            parts[0].toInt() to parts[1].trim()
        }.toMap()

        val regexString = buildRegex("( 8 ) + ( 42 ){n} ( 31 ){n}", ruleMap)

        return lines.count { line ->
            (1..10).toList().any { count ->
                val regex = regexString.replace("n", "$count").toRegex()
                line.matches(regex)
            }
        }
    }

    fun buildRegex(start: String, ruleMap: Map<Int, String>): String {
        var regex = start
        while (regex.any { it.isDigit() })
        {
            var newState = ""
            regex.split(" ").forEach {
                if (it.contains("\"") || it.contains("(") || it.contains(")") || it.contains("|") || it.contains("+"))
                {
                    newState += "$it "
                }
                else
                {
                    val subRule = ruleMap[it.toInt()]
                    if (subRule!!.contains("|"))
                    {
                        newState += "( $subRule ) "
                    }
                    else
                    {
                        newState += "$subRule "
                    }
                }
            }
            regex = newState.trim()
        }

        return regex.replace(" ", "").replace("\"", "")
    }
}
