package nielskooij.adventofcode.twenty

object Questions {

    fun countAllAnswers(input: String): Int =
        input.split(System.lineSeparator() + System.lineSeparator())
             .map { it.split(System.lineSeparator()) }
             .map { group -> group.fold(setOf<Char>()) { acc, i -> acc.union(i.toCharArray().asIterable()) } }
             .map { it.size }
             .sum()

    fun countIntersectingAnswers(input: String): Int =
        input.split(System.lineSeparator() + System.lineSeparator())
            .map { it.split(System.lineSeparator()) }
            .map { group -> group.fold(('a'..'z').toSet()) { acc, i -> acc.intersect(i.toCharArray().asIterable()) } }
            .map { it.size }
            .sum()

}