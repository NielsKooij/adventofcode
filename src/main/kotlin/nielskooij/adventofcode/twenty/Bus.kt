package nielskooij.adventofcode.twenty

import java.math.BigInteger

object Bus {

    fun firstDeparture(input: List<String>): Long {
        val earliestTimestamp = input[0].toLong()
        val busses = input[1].split(",").filter { it != "x" }.map { it.toInt() }

        var time = earliestTimestamp
        while (true) {
            busses.forEach { bus ->
                if(time % bus == 0L) return (time - earliestTimestamp) * bus
            }
            time++
        }
    }

    fun firstSubsequentDeparture(input: List<String>): String {
        val busses = input[1].split(",")
            .mapIndexed { i, b -> Pair(b, i.toLong())
            }.filter { it.first != "x"
            }.map { Pair(BigInteger.valueOf(it.first.toLong()), BigInteger.valueOf(it.second)) }

        var time: BigInteger = BigInteger.valueOf(0L)
        var increment = BigInteger.ONE

        busses.forEach { bus ->
            while ((time + bus.second) % bus.first != BigInteger.ZERO) {
                time += increment
            }
            increment *= bus.first
        }
        return time.toString()
    }
}