package nielskooij.adventofcode.twenty

import java.lang.Long.max

object Jolts {


    fun joltDifference(input: List<Int>): Int {
        val sorted = input.sorted().reversed()
        val device = input.maxOf { it } + 3

        return countDiff(device, 1, sorted) * countDiff(device, 3, sorted)
    }

    private fun countDiff(start: Int, diff: Int, input: List<Int>): Int =
        input.fold(Pair(start, 0)) { acc, i ->
            if (acc.first - i == diff)
                Pair(i, acc.second + 1)
            else
                Pair(i, acc.second)
        }.second

    fun arrange(input: List<Int>): Long {
        val adapters = input.sorted().reversed()
            .map { Pair<Int, Long?>(it, null) }.toMutableList()

        for(i in adapters.indices) {
            val nrFromHere = (i - 3 until i).toList().mapNotNull {
                if(it >= 0 && (adapters[it].first - adapters[i].first <= 3)) adapters[it].second
                else null
            }
            adapters[i] = Pair(adapters[i].first, max(nrFromHere.sum(), 1L))
        }

        return adapters[adapters.size - 1].second!!
    }
}