package nielskooij.adventofcode.twenty

import java.lang.NumberFormatException

object Passport {

    private val requiredKeys: Map<String, (String) -> Boolean> = mapOf(
        "byr" to yearPredicate(1920, 2002),
        "iyr" to yearPredicate(2010, 2020),
        "eyr" to yearPredicate(2020, 2030),
        "hgt" to heightPredicate(),
        "hcl" to { v -> v.matches("#[0-9a-f]{6}".toRegex()) },
        "ecl" to { v -> v in listOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth") },
        "pid" to { v -> v.matches("[0-9]{9}".toRegex()) }
    )

    fun countValid(input: String, predicate: (Map<String, String>) -> Boolean): Int =
        input.split(System.lineSeparator() + System.lineSeparator())
            .map { parsePassport(it) }
            .count { predicate(it) }

    fun parsePassport(passportInput: String): Map<String, String> =
        passportInput
            .replace(System.lineSeparator(), " ")
            .split(" ")
            .map {
                it.split(":")
                    .let { pair -> pair[0] to pair[1] }
            }
            .toMap()

    fun containsRequiredFields(passport: Map<String, String>): Boolean =
        requiredKeys.keys.intersect(passport.keys).size == requiredKeys.size

    fun isValidPassport(passport: Map<String, String>): Boolean =
        containsRequiredFields(passport) &&
        requiredKeys.map { entry -> (entry.value.invoke(passport[entry.key]!!)) }
                    .all { it }

    private fun yearPredicate(min: Int, max: Int): (String) -> Boolean =
        { v ->
            v.toIntOrNull()
                ?.let { it in min..max }
                ?: false
        }

    private fun heightPredicate(): (String) -> Boolean =
        { v ->
            try {
                when {
                    v.endsWith("cm") -> v.replace("cm", "").toInt()
                        .let { it in 150..193 }
                    v.endsWith("in") -> v.replace("in", "").toInt()
                        .let { it in 59..76 }
                    else -> false
                }
            } catch (e: NumberFormatException) {
                false
            }
        }
}