package nielskooij.adventofcode.twenty

object SpokenNumbers
{

    fun nthSpokenNumber(input: String, n: Int): Int {
        val spokenNumbers = mutableMapOf<Int, Int>()
        input.split(",").forEachIndexed { index, n -> spokenNumbers[n.toInt()] = index + 1 }
        val size = spokenNumbers.size

        var lastNumberSpoken = -1
        for (i in size until n) {
            val lastIndex = spokenNumbers[lastNumberSpoken]
            spokenNumbers[lastNumberSpoken] = i

            lastNumberSpoken = if(lastIndex == null) 0 else i - lastIndex
        }

        return lastNumberSpoken
    }
}
