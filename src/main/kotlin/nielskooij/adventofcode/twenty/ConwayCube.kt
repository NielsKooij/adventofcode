package nielskooij.adventofcode.twenty

import nielskooij.adventofcode.util.add

object ConwayCube
{

    fun calculateActive(input: List<String>, nrOfIterations: Int): Long {
        var currentState = parseInput(input)
        for (i in 0 until nrOfIterations) {
            var adjacentCellsToCheck = listOf<P>()

            val newState = currentState.mapNotNull { (k, _) ->
                val adjacentCells = adjacentCells(k)
                adjacentCellsToCheck = adjacentCellsToCheck.plus(adjacentCells)

                if (newState(true, adjacentStates(adjacentCells, currentState))) k to true
                else null
            }.toMap()

            val newlyAddedPoints = adjacentCellsToCheck.filter {
                currentState[it] == null
            }.mapNotNull { cell ->
                if (newState(false, adjacentStates(cell, currentState))) cell to true
                else null
            }.toMap()

            currentState = newState.plus(newlyAddedPoints)
        }

        return currentState.size.toLong()
    }

    fun parseInput(input: List<String>): Map<P, Boolean> =
        input.flatMapIndexed { x, line ->
            line.toCharArray().mapIndexed { y, char ->
                if (char == '#') P(x, y, 0) to true
                else null
            }
        }.filterNotNull().toMap()

    fun newState(state: Boolean, adjacentCells: List<Boolean>): Boolean =
        when(state) {
            true -> adjacentCells.count { it } in 2..3
            false -> adjacentCells.count { it } == 3
        }

    fun <T> adjacentStates(cells: List<T>, state: Map<T, Boolean>): List<Boolean> =
        cells.map { state.getOrElse(it) { false } }

    fun adjacentStates(cell: P, state: Map<P, Boolean>): List<Boolean> =
        adjacentStates(adjacentCells(cell), state)

    fun adjacentCells(cell: P): List<P> =
        combinations3D((-1..1).toList()).map { vector ->
            cell.add(vector)
        }

    fun combinations3D(list: List<Int>): List<P> =
        list.flatMap { a ->
            list.flatMap { b ->
                list.map { c -> Triple(a, b, c)
                }
            }
        }.filter { it != Triple(0,0,0) }

    fun calculateActive4D(input: List<String>, nrOfIterations: Int): Long {
        var currentState = parseInput4D(input)
        for (i in 0 until nrOfIterations) {
            var adjacentCellsToCheck = listOf<Quadruple>()

            val newState = currentState.mapNotNull { (k, _) ->
                val adjacentCells = adjacentCells4D(k)
                adjacentCellsToCheck = adjacentCellsToCheck.plus(adjacentCells)

                if (newState(true, adjacentStates(adjacentCells, currentState))) k to true
                else null
            }.toMap()

            val newlyAddedPoints = adjacentCellsToCheck.filter {
                currentState[it] == null
            }.mapNotNull { cell ->
                if (newState(false, adjacentStates4D(cell, currentState))) cell to true
                else null
            }.toMap()

            currentState = newState.plus(newlyAddedPoints)
        }

        return currentState.size.toLong()
    }

    fun parseInput4D(input: List<String>): Map<Quadruple, Boolean> =
        input.flatMapIndexed { x, line ->
            line.toCharArray().mapIndexed { y, char ->
                if (char == '#') Quadruple(x, y, 0, 0) to true
                else null
            }
        }.filterNotNull().toMap()

    fun adjacentStates4D(cell: Quadruple, state: Map<Quadruple, Boolean>): List<Boolean> =
        adjacentStates(adjacentCells4D(cell), state)

    fun adjacentCells4D(cell: Quadruple): List<Quadruple> =
        combinations4D((-1..1).toList()).map { vector ->
            cell.add(vector)
        }

    fun combinations4D(list: List<Int>): List<Quadruple> =
        list.flatMap { w ->
            list.flatMap { x ->
                list.flatMap { y ->
                    list.map { z ->
                        Quadruple(w, x, y, z)
                    }
                }
            }
        }.filter { it != Quadruple(0, 0, 0, 0) }

}

typealias P = Triple<Int, Int, Int>

data class Quadruple(val w: Int, val x: Int, val y: Int, val z: Int) {

    fun add(other: Quadruple): Quadruple =
        Quadruple(w + other.w, x + other.x, y + other.y, z + other.z)
}
