package nielskooij.adventofcode.twenty

object Password
{

    fun countValid(input: List<String>, predicate: (ParsedLine) -> Boolean) =
        input.map { toParsedLine(it) }
             .count(predicate)

    fun maxOcurrence(line: ParsedLine): Boolean =
        line.password.fold(0) { acc, c -> if (c == line.character) acc + 1 else acc } in (line.i)..(line.j)

    fun xorIndex(line: ParsedLine): Boolean =
        (line.password.toCharArray()[line.i] == line.character).xor(line.password.toCharArray()[line.j] == line.character)

    private fun toParsedLine(input: String): ParsedLine {
        val parts1 = input.split(":")
        val parts2 = parts1[0].split(" ")
        val parts3 = parts2[0].split("-")

        return ParsedLine(parts1[1], parts2[1].single(), parts3[0].toInt(), parts3[1].toInt())
    }

    data class ParsedLine(val password: String, val character: Char, val i: Int, val j: Int)

}
