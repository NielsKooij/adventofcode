package nielskooij.adventofcode.twenty

object Seat {

    fun calculateHighestSeatId(input: List<String>): Int =
        input.map { calculateSeatId(it) }
             .maxOf { it }

    fun findMissingSeat(input: List<String>): Int =
        input.map { calculateSeatId(it) }
             .sorted()
             .fold(Pair(0, 0)) {
                     acc, i -> Pair(i, if (acc.first + 1 == i) acc.second else i - 1)
             }.second

    fun calculateSeatId(seat: String): Int =
        seat.map { if (it == 'F' || it == 'L') 0 else 1 }
            .reversed()
            .fold(Pair(0, 1)) { acc, i -> Pair(acc.first + i * acc.second, acc.second * 2) }.first

}