package nielskooij.adventofcode.twenty

object Sum
{

    fun sum2020(input: List<Int>): Int =
        input
            .partition { it > 1010 }
            .let { it.first.flatMap { t -> it.second.map { u -> Pair(t, u) } } } // combinations
            .filter { it.first + it.second == 2020 }
            .map { it.first * it.second }
            .single()

    fun sum2020x(input: List<Int>): Int =
        input
            .let { it.flatMap { x -> it.flatMap { y -> it.map { z -> listOf(x, y, z) } } } }
            .filter { it.sum() == 2020 }
            .map { it.reduce { acc, i -> i * acc } }
            .first()
}
