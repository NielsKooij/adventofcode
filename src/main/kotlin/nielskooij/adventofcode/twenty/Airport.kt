package nielskooij.adventofcode.twenty

object Airport {

    fun countTrees(input: List<String>): Int =
        input.map { line -> line.toCharArray().map { it == '#' }.toTypedArray() }.toTypedArray()
            .let { countTrees(it, Pair(0, 0), Pair(1, 3), 0) }

    fun countTrees2(input: List<String>): Long =
        input.map { line -> line.toCharArray().map { it == '#' }.toTypedArray() }.toTypedArray()
            .let { i ->
                listOf(Pair(1, 1), Pair(1, 3), Pair(1, 5), Pair(1, 7), Pair(2, 1))
                    .map { countTrees(i, Pair(0, 0), it, 0) }
                    .fold(1L) { acc, i -> acc * i }
            }

    fun countTrees(treeGrid: Array<Array<Boolean>>, pos: Pair<Int, Int>, move: Pair<Int, Int>, count: Int): Int =
        if (pos.first >= treeGrid.size) count
        else countTrees(
            treeGrid,
            nextPos(treeGrid[0].size, pos, move),
            move,
            if (hasTree(treeGrid, pos)) count + 1 else count
        )

    fun nextPos(gridWidth: Int, pos: Pair<Int, Int>, move: Pair<Int, Int>): Pair<Int, Int> =
        Pair(pos.first + move.first, (pos.second + move.second) % gridWidth)

    fun hasTree(treeGrid: Array<Array<Boolean>>, pos: Pair<Int, Int>): Boolean =
        treeGrid[pos.first][pos.second]

}