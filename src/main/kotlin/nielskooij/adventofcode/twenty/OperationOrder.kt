package nielskooij.adventofcode.twenty

import java.math.BigInteger

object OperationOrder
{

    fun evalAll(input: List<String>): BigInteger =
        input.map { eval(it.replace(" ", "")) }.sumOf { it }

    fun eval(expression: String): BigInteger =
        parseExpressionBest(expression.reversed(), BigInteger.ZERO).first

    private fun parseExpressionBest(expression: String, state: BigInteger): Pair<BigInteger, String> =
        if (expression.isEmpty()) Pair(state, "")
        else
        {
            if (expression[0].isDigit())
                parseExpressionBest(expression.substring(1), BigInteger.valueOf(expression[0].toString().toLong()))
            else when (expression[0])
            {
                '+' -> parseExpressionBest(expression.substring(1), BigInteger.ZERO).let {
                    Pair(state + it.first, it.second)
                }
                '*' -> parseExpressionBest(expression.substring(1), BigInteger.ZERO).let {
                    Pair(state * it.first, it.second)
                }
                ')' -> parseExpressionBest(expression.substring(1), BigInteger.ZERO).let {
                    parseExpressionBest(it.second, it.first)
                }
                '(' -> Pair(state, expression.substring(1))
                else -> throw IllegalStateException("At: $expression")
            }
        }

    fun evalAllPrecedence(input: List<String>): BigInteger =
        input.map { evalPrecedence(it.replace(" ", "")) }.sumOf { it }

    fun evalPrecedence(expression: String): BigInteger =
        parseExpressionPrecedence(expression)

    private fun parseExpressionPrecedence(expression: String): BigInteger =
        if (expression.takeWhile { it.isDigit() }.length == expression.length) BigInteger.valueOf(expression.toLong())
        else
        {
            if (expression.indexOf("(") != -1) {
                val subExprIndex = expression.indexOf("(")
                var opening = 0
                var newExpr = ""
                for (i in (subExprIndex + 1) until expression.length) {
                    if (expression[i] == '(') opening++
                    else if (opening == 0 && expression[i] == ')')
                    {
                        val subExpression = parseExpressionPrecedence(expression.substring(subExprIndex + 1, i))
                        newExpr = expression.substring(0, subExprIndex) + subExpression.toString() + expression.substring(i + 1)
                    }
                    else if(expression[i] == ')') opening--
                }
                parseExpressionPrecedence(newExpr)
            }
            else if (expression.indexOf("+") != -1) {
                val sumExprIndex = expression.indexOf("+")

                val leftPart = expression.substring(0, sumExprIndex)
                val rightPart = expression.substring(sumExprIndex + 1)

                val left = parseExpressionPrecedence(leftPart.takeLastWhile { it.isDigit() })
                val right = parseExpressionPrecedence(rightPart.takeWhile { it.isDigit() })
                val newExpr = expression.substring(0, sumExprIndex - leftPart.takeLastWhile { it.isDigit() }.length) +
                        (left + right).toString() +
                        expression.substring(sumExprIndex + 1 + rightPart.takeWhile { it.isDigit() }.length)
                parseExpressionPrecedence(newExpr)
            }
            else if (expression.indexOf("*") != -1) {
                val prodExprIndex = expression.indexOf("*")

                val leftPart = expression.substring(0, prodExprIndex)
                val rightPart = expression.substring(prodExprIndex + 1)

                val left = parseExpressionPrecedence(leftPart.takeLastWhile { it.isDigit() })
                val right = parseExpressionPrecedence(rightPart.takeWhile { it.isDigit() })
                val newExpr = expression.substring(0, prodExprIndex - leftPart.takeLastWhile { it.isDigit() }.length) +
                        (left * right).toString() +
                        expression.substring(prodExprIndex + 1 + rightPart.takeWhile { it.isDigit() }.length)
                parseExpressionPrecedence(newExpr)
            }
            else throw IllegalStateException("Unable to parse $expression")
        }
}



