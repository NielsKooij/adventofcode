package nielskooij.adventofcode.twenty

import java.lang.Exception
import java.lang.IllegalStateException

object Program {

    fun findStateAtLoop(input: List<String>): Long =
        try {
            val state = runProgram(parseProgram(input))
            state.acc
        } catch (e: LoopException) {
            e.state.acc
        }

    fun runProgram(state: State<MutableList<Long>>, program: Array<Operation>): State<MutableList<Long>> =
        if (state.state.contains(state.pc))
            throw LoopException(state, "Loop found")
        else
            when (program[state.pc.toInt()].operation) {
                "acc" -> State(
                    state.pc + 1,
                    state.acc + program[state.pc.toInt()].value,
                    state.state.also { it.add(state.pc) })

                "jmp" -> State(
                    state.pc + program[state.pc.toInt()].value.toInt(),
                    state.acc,
                    state.state.also { it.add(state.pc) })

                "nop" -> State(
                    state.pc + 1,
                    state.acc,
                    state.state.also { it.add(state.pc) })

                else -> throw IllegalStateException("${program[state.pc.toInt()].operation} is not an operation")
            }

    fun repair(input: List<String>): Long {
        val program = parseProgram(input)

        lateinit var op: Operation
        for (i in program.indices) {
            op = program[i]

            try {
                when(op.operation) {
                    "nop" -> {
                        program[i] = Operation("jmp", op.value)
                        return runProgram(program).acc
                    }
                    "jmp" -> {
                        program[i] = Operation("nop", op.value)
                        return runProgram(program).acc
                    }
                }
            } catch (e: LoopException) {
                program[i] = op
            }
        }
        return -1
    }

    private fun parseProgram(input: List<String>): Array<Operation> =
        input.map {
            val parts = it.split(" ")
            Operation(parts[0], parts[1].replace("+", "").toLong())
        }.toTypedArray()

    private fun runProgram(program: Array<Operation>): State<MutableList<Long>> {
        var state = State<MutableList<Long>>(0, 0, mutableListOf())

        while (state.pc < program.size) {
            state = runProgram(state, program)
        }

        return state
    }

    data class Operation(val operation: String, val value: Long)

    data class State<T>(val pc: Long, val acc: Long, val state: T)

    class LoopException(val state: State<MutableList<Long>>, message: String) : Exception(message)

}