package nielskooij.adventofcode.twenty

object XMAS {

    private const val preamble = 25
    private const val invalidNumber = 1504371145L
    private const val indexOfInvalidNumber = 652

    fun crackCipher(input: List<Long>): Long {
        for (i in preamble until input.size) {
            if(!hasSum(input[i], i - preamble, input)) return input[i]
        }

        return -1
    }

    fun hasSum(elem: Long, startIndex: Int, input: List<Long>): Boolean {
        for (i in startIndex until (startIndex + preamble)) {
            for (j in i + 1 until startIndex + preamble) {
                if (input[i] + input[j] == elem) return true
            }
        }
        return false
    }

    fun crackCypher2(input: List<Long>): Long {
        var i = indexOfInvalidNumber - 1
        var size = 2

        var sum = sum(i - size, i, input)
        while (sum != invalidNumber) {
            if (sum > invalidNumber) {
                i--
                size = 2
            } else {
                size++
            }

            sum = sum(i - size, i, input)
        }

        val subList = input.subList(i - size, i)
        return subList.minOf { it } + subList.maxOf { it }
    }

    fun sum(start: Int, end: Int, input: List<Long>) =
        input.subList(start, end).sum()

}