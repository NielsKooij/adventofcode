package nielskooij.adventofcode.twenty

import nielskooij.adventofcode.common.Matrix
import nielskooij.adventofcode.util.add
import java.lang.IllegalStateException

object FerryHallSeat {

    fun calculateOccupiedSeats(input: List<String>): Int {
        val matrix = parseMatrix(input)

        val equilibrium = doIterations(Matrix(listOf(), 0, 0), matrix) { matrix, cell ->
            val dim: Pair<Int, Int> = matrix.indexToDim(cell.first)
            Pair(cell.first, newSeatState(4, cell.second, matrix.getAdjacentCells(dim).map { it.second }))
        }

        return equilibrium.map { _, it -> it.second == SeatState.FULL }.cells.count { it }
    }

    fun calculateOccupiedSeats2(input: List<String>): Int {
        val matrix = parseMatrix(input)

        val equilibrium = doIterations(Matrix(listOf(), 0, 0), matrix) { matrix, cell ->
            val dim: Pair<Int, Int> = matrix.indexToDim(cell.first)
            val adjacent = matrix.getAdjacentCells(dim) { m, c ->
                allDirections().map { direction ->
                    findFirst(m, c, direction)
                }
            }

            Pair(cell.first, newSeatState(5, cell.second, adjacent.map { it.second }))
        }

        return equilibrium.map { _, it -> it.second == SeatState.FULL }.cells.count { it }
    }

    fun findFirst(
        matrix: Matrix<Pair<Int, SeatState>>,
        start: Pair<Int, Int>,
        direction: Pair<Int, Int>
    ): Pair<Int, Int> {
        var next = start.add(direction)

        while (true) {
            if (matrix.get(next)?.second != SeatState.FLOOR || next.first !in 0 until matrix.m || next.second !in 0 until matrix.n) break
            next = next.add(direction)
        }

        return next
    }

    private fun allDirections(): List<Pair<Int, Int>> = listOf(
        Pair(-1, -1),
        Pair(-1,  0),
        Pair(-1,  1),
        Pair( 0, -1),
        Pair( 0,  1),
        Pair( 1, -1),
        Pair( 1,  0),
        Pair( 1,  1)
    )

    private fun doIterations(
        oldState: Matrix<Pair<Int, SeatState>>,
        newState: Matrix<Pair<Int, SeatState>>,
        iterFun: (Matrix<Pair<Int, SeatState>>, Pair<Int, SeatState>) -> Pair<Int, SeatState>
    ): Matrix<Pair<Int, SeatState>> =
        if (oldState.cells == newState.cells) newState
        else doIterations(newState, newState.map(iterFun), iterFun)

    private fun newSeatState(occupiedToEmpty: Int, oldSeatState: SeatState, adjacent: List<SeatState>): SeatState =
        when (oldSeatState) {
            SeatState.EMPTY -> if (adjacent.none { it == SeatState.FULL }) SeatState.FULL else SeatState.EMPTY
            SeatState.FULL -> if (adjacent.count { it == SeatState.FULL } >= occupiedToEmpty) SeatState.EMPTY else SeatState.FULL
            else -> oldSeatState
        }

    fun parseMatrix(input: List<String>): Matrix<Pair<Int, SeatState>> {
        val state: Array<Array<SeatState>> = input.map {
            it.toCharArray().map { c ->
                when (c) {
                    '#' -> SeatState.FULL
                    'L' -> SeatState.EMPTY
                    '.' -> SeatState.FLOOR
                    else -> throw IllegalStateException("Unknown character $c")
                }
            }.toTypedArray()
        }.toTypedArray()

        val indexedState = (0 until (state.size * state[0].size)).zip(state.flatMap { it.toList() })
        return Matrix(indexedState, state.size, state[0].size)
    }

    enum class SeatState {
        FLOOR,
        FULL,
        EMPTY;

        override fun toString(): String =
            when (this) {
                FLOOR -> "."
                FULL -> "#"
                EMPTY -> "L"
            }
    }
}