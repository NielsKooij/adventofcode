package nielskooij.adventofcode.twentyone

class Day5 : Day(2021, 5) {
    override fun part1(input: String): Any = parse(input)
        .flatMap(Line::getPoints)
        .groupingBy { it }
        .eachCount()
        .count { it.value > 1 }

    override fun part2(input: String): Any = parse(input)
        .flatMap { it.getPoints(true) }
        .groupingBy { it }
        .eachCount()
        .count { it.value > 1 }

    // =================== Parsing ==========================================
    private fun parse(input: String): List<Line> = input.lines()
        .map {
            val (start, end) = it.split(" -> ")
            val (startX, startY) = start.split(",").map(String::toInt)
            val (endX, endY) = end.split(",").map(String::toInt)
            Line(Coord(startX, startY), Coord(endX, endY))
        }
}

data class Coord(val x: Int, val y: Int)

class Line(val start: Coord, val end: Coord) {
    fun getPoints(includeDiagonal: Boolean = false): List<Coord> =
        if (start.x != end.x && start.y != end.y) {
            if (includeDiagonal) getPointsDiagonal() else listOf()
        } else if (start.x != end.x) getPointsHorizontal()
        else getPointsVertical()

    private fun getPointsHorizontal(): List<Coord> =
        (if (start.x < end.x) (start.x..end.x)
        else (end.x..start.x))
            .map { Coord(it, start.y) }

    private fun getPointsVertical(): List<Coord> =
        (if (start.y < end.y) (start.y..end.y)
        else (end.y..start.y))
            .map { Coord(start.x, it) }

    private fun getPointsDiagonal(): List<Coord> =
        if (start.x < end.x) (start.x..end.x).mapIndexed { i, x ->
            if(start.y < end.y) Coord(x, start.y + i)
            else Coord(x, start.y - i)
        }
        else (end.x..start.x).mapIndexed { i, x ->
            if(end.y < start.y) Coord(x, end.y + i)
            else Coord(x, end.y - i)
        }
}