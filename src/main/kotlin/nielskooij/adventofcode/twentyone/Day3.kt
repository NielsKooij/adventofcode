package nielskooij.adventofcode.twentyone

import nielskooij.adventofcode.common.zipWith
import kotlin.math.pow

class Day3 : Day(2021, 3) {

    override fun part1(input: String): Any = input.lineSequence()
        .map(String::toCharArray)
        .fold(listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) { acc, t ->
            acc.zipWith(t.map(::toVector), Int::plus)
        }
        .let { n -> toInt(n) * toInt(n, true) }

    override fun part2(input: String): Any = input.lines()
        .map(String::toCharArray)
        .map { it.map { c -> toVector(c) } }
        .let { ns ->
            val  oxygen = findRating(ns)
            val co2 = findRating(ns, isOxygen = false)
            oxygen * co2
        }

    private fun toVector(c: Char): Int = when (c) {
        '0' -> -1
        '1' -> 1
        else -> throw IllegalArgumentException("Character must be '0' or '1'")
    }

    private fun toInt(ns: List<Int>, inverted: Boolean = false): Int = ns
        .reversed()
        .map { if(inverted) -it else it }
        .foldIndexed(0) { i, acc, t ->
            if (t > 0) (acc + 2f.pow(i)).toInt() else acc
        }

    private fun findRating(numbers: List<List<Int>>, index: Int= 0, isOxygen: Boolean = true): Int =
        if(numbers.size == 1) toInt(numbers[0])
        else numbers
        .fold(listOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)) { acc, t ->
            acc.zipWith(t, Int::plus)
        }.let { n ->
            if (n[index] == 0) {
                val expected = if(isOxygen) 1 else -1
                findRating(filterBitOnIndex(numbers, index, expected), index + 1, isOxygen)
            } else if ((isOxygen && n[index] > 0) || (!isOxygen && n[index] < 0)) {
                findRating(filterBitOnIndex(numbers, index, 1), index + 1, isOxygen)
            } else {
                findRating(filterBitOnIndex(numbers, index, -1), index + 1, isOxygen)
            }
        }

    private fun filterBitOnIndex(numbers: List<List<Int>>, index: Int, expected: Int): List<List<Int>> =
        numbers.filter { n -> n[index] == expected }
}