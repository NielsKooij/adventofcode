package nielskooij.adventofcode.twentyone

import nielskooij.adventofcode.common.ResourceHelper

abstract class Day(private val year: Int, private val day: Int)
{
    private val input = ResourceHelper.getResourceAsString("/$year/Day$day.txt")

    abstract fun part1(input: String): Any

    abstract fun part2(input: String): Any

    fun run() {
        println("$year.$day.1: ${part1(input)}")
        println("$year.$day.2: ${part2(input)}")
    }
}
