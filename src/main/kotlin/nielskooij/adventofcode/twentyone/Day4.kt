package nielskooij.adventofcode.twentyone

import nielskooij.adventofcode.common.split

class Day4 : Day(2021, 4) {

    override fun part1(input: String): Any {
        val bingo = parse(input)
        var boards = bingo.boards
        var score: Int? = null
        var i = 0

        while (score == null) {
            val n = bingo.calledNumbers[i]
            boards = boards.map { board ->
                val newBoard = addNumberToBoard(n, board)

                val isWinner = isWinner(newBoard)
                if (isWinner) {
                    score = calculateScore(n, newBoard)
                }
                newBoard
            }
            i++
        }

        return score!!
    }

    private fun addNumberToBoard(n: Int, board: Board): Board = board
        .map { row ->
            row.map { i ->
                if (i == n) null else i
            }
        }

    private fun isWinner(board: Board): Boolean {
        val size = board[0].size
        for (i in (0 until size)) {
            if (board
                    .map { it[i] }
                    .all { it == null }
            ) return true
        }

        return board.any { l -> l.all { it == null } }
    }

    private fun calculateScore(lastNumber: Int, board: Board): Int =
        board.sumOf { row -> row.filterNotNull().sum() } * lastNumber

    override fun part2(input: String): Any {
        val bingo = parse(input)
        var boards = bingo.boards
        var i = 0
        var lastWinner: Board? = null

        while (boards.isNotEmpty()) {
            val n = bingo.calledNumbers[i]
            boards = boards
                .mapNotNull { b ->
                    val newBoard = addNumberToBoard(n, b)
                    if(isWinner(newBoard)) {
                        lastWinner = newBoard
                        null
                    } else newBoard
                }
            i++
        }

        return calculateScore(bingo.calledNumbers[i - 1], lastWinner!!)
    }

    // =================== Parsing ==========================================

    private fun parse(input: String): Bingo = input.lines()
        .split { it == "" }
        .let { list ->
            val numbers = list[0][0].split(",").map(String::toInt)
            val boards = list.subList(1, list.size).map(::parseBoard)
            Bingo(numbers, boards)
        }

    private fun parseBoard(rawBoard: List<String>): Board = rawBoard
        .map { row ->
            row
                .split(" ")
                .filter { it != "" }
                .map(String::toInt)
        }
}

data class Bingo(
    val calledNumbers: List<Int>,
    val boards: List<Board>
)

typealias Board = List<List<Int?>>