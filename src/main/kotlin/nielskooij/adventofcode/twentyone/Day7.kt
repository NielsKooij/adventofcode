package nielskooij.adventofcode.twentyone

import kotlin.math.abs

class Day7 : Day(2021, 7)
{
    override fun part1(input: String): Any = parse(input)
        .minFuel { it }

    override fun part2(input: String): Any = parse(input)
        .minFuel { n -> (n * (n + 1)) / 2 }

    private fun List<Int>.minFuel(f: (n: Int) -> Int): Int = this.sorted().let { ns ->
        (ns.first()..ns.last()).minOf { i ->
            ns.sumOf {
                f.invoke(abs(i - it))
            }
        }
    }

    private fun parse(input: String): List<Int> = input.replace("\n", "")
        .split(",")
        .map(String::toInt)
}
