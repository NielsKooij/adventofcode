package nielskooij.adventofcode.twentyone

class Day2 : Day(2021, 2)
{
    override fun part1(input: String) = parse(input)
            .fold(Pair(0, 0)) { acc, move ->
                when (move)
                {
                    is Forward -> Pair(acc.first + move.amount, acc.second)
                    is Up      -> Pair(acc.first, acc.second - move.amount)
                    is Down    -> Pair(acc.first, acc.second + move.amount)
                }
            }.let { it.first * it.second }

    override fun part2(input: String) = parse(input)
            .fold(Triple(0, 0, 0)) { acc, move ->
                when (move)
                {
                    is Forward -> Triple(acc.first + move.amount, acc.second, acc.third + (acc.second * move.amount))
                    is Up      -> Triple(acc.first, acc.second - move.amount, acc.third)
                    is Down    -> Triple(acc.first, acc.second + move.amount, acc.third)
                }
            }.let { it.first * it.third }


    private fun parse(input: String): List<Move> = input.lines()
        .map { line ->
            line.split(" ").let {
                when (it[0])
                {
                    "forward" -> Forward(it[1].toInt())
                    "up"      -> Up(it[1].toInt())
                    "down"    -> Down(it[1].toInt())
                    else      -> throw IllegalArgumentException("Unknown move $line")
                }
            }
        }
}

sealed class Move(val amount: Int)

class Forward(amount: Int) : Move(amount)
class Up(amount: Int) : Move(amount)
class Down(amount: Int) : Move(amount)
