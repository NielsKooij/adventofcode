package nielskooij.adventofcode.twentyone

import nielskooij.adventofcode.common.ResourceHelper

fun main() {
    val day1Resource = ResourceHelper.getResourceAsIntList("/2021/Day1.txt")
    println("Day 1: ${Day1.part1(day1Resource)}, ${Day1.part2(day1Resource)}")

    Day2().run()
    Day3().run()
    Day4().run()
    Day5().run()
    Day6().run()
    Day7().run()
}
