package nielskooij.adventofcode.twentyone

object Day1 {

    fun part1(input: List<Int>): Int =
        input.fold(Pair(0, 0)) { acc, i ->
            if (i > acc.second) Pair(acc.first + 1, i) else Pair(acc.first, i)
        }.first - 1

    fun part2(input: List<Int>): Int =
        input.windowed(3, 1).fold(Pair(0, 0)) { acc, i ->
            val sum = i.sum()
            if (sum > acc.second) Pair(acc.first + 1, sum) else Pair(acc.first, sum)
        }.first - 1
}