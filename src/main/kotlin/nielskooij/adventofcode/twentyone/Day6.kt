package nielskooij.adventofcode.twentyone

import nielskooij.adventofcode.common.sumBy

class Day6 : Day(2021, 6)
{
    override fun part1(input: String): Any =
        fishesAfter(80, parse(input)).sumBy(Fish::amount)

    override fun part2(input: String): Any =
        fishesAfter(256, parse(input)).sumBy(Fish::amount)

    private fun fishesAfter(n: Int, initial: List<Fish>): List<Fish> =
        (0 until n).fold(initial) { acc, _ ->
            acc.flatMap(Fish::tick)
                .groupBy(Fish::timer)
                .map { (timer, fishes) -> Fish(timer, fishes.sumBy(Fish::amount)) }
        }

    private fun parse(input: String): List<Fish> = input.replace("\n", "")
        .split(",")
        .map { Fish(it.toLong(), 1) }
}

typealias Fish = Pair<Long, Long>

fun Fish.timer() = this.first
fun Fish.amount() = this.second

fun Fish.tick(): List<Fish> =
    if (timer() == 0L) listOf(Fish(6, amount()), Fish(8, amount()))
    else listOf(Fish(timer() - 1, amount()))
