package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day
import kotlin.math.pow

class Day4 : Day(2023, 4) {

    override fun part1(input: String) = parseInput(input)
        .map(::calculateNrOfWinningNumbers)
        .sumOf {
            if (it > 0) 2.0.pow(it.toDouble() - 1).toInt()
            else 0
        }

    private fun calculateNrOfWinningNumbers(card: Card) =
        card.winningNumbers.intersect(card.myNumbers.toSet()).size

    override fun part2(input: String) = parseInput(input).let {
        val size = it.size

        it
            .fold(initializeCardMultiplierList(size)) { acc, card ->
                val winnings = calculateNrOfWinningNumbers(card)

                val multiplier = acc[card.cardNumber - 1]

                (card.cardNumber until (card.cardNumber + winnings)).forEach { cardNr ->
                    acc[cardNr] += multiplier
                }

                acc
            }
            .sum()
    }

    private fun initializeCardMultiplierList(size: Int): MutableList<Int> =
        (0 until size).map { 1 }.toMutableList()

    private fun parseInput(input: String) = input.lines()
        .map { line ->
            val parts = line.split(":")
            val cardNumber = parts[0].replace("Card", "").trim().toInt()

            val numberParts = parts[1].split("|")
            val winningNumbers = numberParts[0]
                .split(" ")
                .filter(String::isNotEmpty)
                .map(String::toInt)
            val myNumbers = numberParts[1]
                .split(" ")
                .filter(String::isNotEmpty)
                .map(String::toInt)

            Card(cardNumber, winningNumbers, myNumbers)
        }
}

data class Card(
    val cardNumber: Int,
    val winningNumbers: List<Int>,
    val myNumbers: List<Int>
)