package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day
import java.lang.IllegalStateException

class Day10: Day(2023, 10) {

    override fun part1(input: String): Any {
        val nodes = parseInput(input)

        var currentNode = nodes.find { row -> row.any { it.symbol == 'S' } }
            ?.find { it.symbol == 'S' }
            ?: throw IllegalStateException("")
        var previousDirection = Direction.WEST
        var count = 0
        do {
            val nextDirection = currentNode.directions.first { it != previousDirection.opposite() }
            val nextPosition = currentNode.position.add(nextDirection.vector)

            currentNode = nodes[nextPosition.y][nextPosition.x]
            previousDirection = nextDirection
            count++
        } while(currentNode.symbol != 'S')

        return count / 2
    }

    override fun part2(input: String) =
        "Not yet implemented"

    private fun parseInput(input: String): List<List<Node>> =
        input.lines()
            .reversed()
            .mapIndexed { rowNr, line -> parseRow(rowNr, line) }

    private fun parseRow(rowNr: Int, row: String): List<Node> =
        row.toCharArray().mapIndexed { columnNr, char ->
            Node(fromSymbol(char), Point(columnNr, rowNr), char)
        }

    private fun fromSymbol(symbol: Char): List<Direction> =
        when(symbol)
        {
            '.' -> listOf(Direction.NONE)
            '|' -> listOf(Direction.NORTH, Direction.SOUTH)
            '-' -> listOf(Direction.EAST, Direction.WEST)
            'L' -> listOf(Direction.NORTH, Direction.EAST)
            'J' -> listOf(Direction.NORTH, Direction.WEST)
            '7' -> listOf(Direction.SOUTH, Direction.WEST)
            'F' -> listOf(Direction.SOUTH, Direction.EAST)
            'S' -> listOf(Direction.NORTH, Direction.WEST, Direction.SOUTH, Direction.EAST)
            else -> throw IllegalStateException("")
        }
}

data class Node(
    val directions: List<Direction>,
    val position: Point,
    val symbol: Char
)

enum class Direction(val vector: Point) {
    NORTH(Point(0, 1)),
    EAST(Point(1, 0)),
    SOUTH(Point(0, -1)),
    WEST(Point(-1, 0)),
    NONE(Point(0, 0));
}

data class Point(
    val x: Int,
    val y: Int
)

fun Point.add(p: Point) =
    Point(this.x + p.x, this.y + p.y)

fun Direction.opposite() =
    when(this)
    {
        Direction.NORTH -> Direction.SOUTH
        Direction.EAST -> Direction.WEST
        Direction.SOUTH -> Direction.NORTH
        Direction.WEST -> Direction.EAST
        Direction.NONE -> Direction.NONE
    }