package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day7: Day(2023, 7) {

    override fun part1(input: String) = parseInput(input, false)
        .sortedWith(HandComparator(false))
        .foldIndexed(0) { index, acc, hand -> acc + ((index + 1) * hand.bidding) }

    override fun part2(input: String) = parseInput(input, true)
        .sortedWith(HandComparator(true))
        .foldIndexed(0) { index, acc, hand -> acc + ((index + 1) * hand.bidding) }

    private fun parseInput(input: String, hasJokerRule: Boolean) = input
        .split("\n")
        .map { it.replace("\r", "") }
        .map { line ->
            val parts = line.split(" ")
            Hand(parts[0], parts[1].toInt(), findType(parts[0], hasJokerRule))
        }

    private fun findType(hand: String, hasJokerRule: Boolean): Type {
        val cardCount = hand
            .toCharArray()
            .fold(mutableMapOf<Char, Int>()) { acc, c ->
                acc.merge(c, 1) { oldValue, value -> oldValue + value }
                acc
            }

        val jokerCount = cardCount['J'] ?: 0
        if(hasJokerRule)
        {
            cardCount.remove('J')
        }

        val type = if (cardCount.containsValue(5)) Type.FIVE_OF_A_KIND
            else if (cardCount.containsValue(4)) Type.FOUR_OF_A_KIND
            else if (cardCount.containsValue(3) && cardCount.containsValue(2)) Type.FULL_HOUSE
            else if (cardCount.containsValue(3)) Type.THREE_OF_A_KIND
            else if (cardCount.count { it.value == 2 } == 2) Type.TWO_PAIR
            else if (cardCount.containsValue(2)) Type.ONE_PAIR
            else Type.HIGH_CARD

        return if(hasJokerRule && jokerCount > 0) {
            when (type)
            {
                Type.HIGH_CARD -> when(jokerCount){
                    1 -> Type.ONE_PAIR
                    2 -> Type.THREE_OF_A_KIND
                    3 -> Type.FOUR_OF_A_KIND
                    else -> Type.FIVE_OF_A_KIND
                }
                Type.ONE_PAIR -> when(jokerCount){
                    1 -> Type.THREE_OF_A_KIND
                    2 -> Type.FOUR_OF_A_KIND
                    else -> Type.FIVE_OF_A_KIND
                }
                Type.TWO_PAIR -> Type.FULL_HOUSE
                Type.THREE_OF_A_KIND -> if(jokerCount == 1) Type.FOUR_OF_A_KIND else Type.FIVE_OF_A_KIND
                Type.FULL_HOUSE -> Type.FULL_HOUSE
                Type.FOUR_OF_A_KIND -> Type.FIVE_OF_A_KIND
                Type.FIVE_OF_A_KIND -> type
            }
        }
        else type
    }
}

data class Hand(
    val cards: String,
    val bidding: Int,
    var type: Type
)

enum class Type{
    HIGH_CARD,
    ONE_PAIR,
    TWO_PAIR,
    THREE_OF_A_KIND,
    FULL_HOUSE,
    FOUR_OF_A_KIND,
    FIVE_OF_A_KIND
}

class HandComparator(private val hasJokerRule: Boolean): Comparator<Hand> {

    override fun compare(h1: Hand, h2: Hand): Int {
        val result = h1.type.ordinal.compareTo(h2.type.ordinal)

        return if(result == 0) replaceOrder(h1.cards).compareTo(replaceOrder(h2.cards))
        else result
    }

    private fun replaceOrder(cards: String): String =
        cards
            .replace("A", "Z")
            .replace("K", "Y")
            .replace("Q", "X")
            .replace("J", if (hasJokerRule) "0" else "W")
            .replace("T", "V")
}
