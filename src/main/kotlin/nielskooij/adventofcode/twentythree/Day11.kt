package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day11 : Day(2023, 11) {

    override fun part1(input: String) = parseInput(input, 1)
        .let { sumOfDistances(it) }


    private fun calculateDistances(fromPoint: Point, toPoints: List<Point>): List<Long> =
        toPoints.map { toPoint -> calculateDistance(fromPoint, toPoint) }

    private fun calculateDistance(p1: Point, p2: Point): Long =
        abs(p1.x.toLong() - p2.x) + abs(p1.y.toLong() - p2.y)

    private fun abs(l: Long) =
        if(l < 0) l * -1 else l
    private fun sumOfDistances(points: List<Point>): Long = points
        .foldIndexed(emptyList<Long>()) { index, acc, point ->
            acc + calculateDistances(point, points.drop(index))
        }.sum()

    override fun part2(input: String) = parseInput(input, 999_999)
        .let { sumOfDistances(it) }

    private fun parseInput(input: String, expansionAddition: Int): List<Point> {
        val lines = input.lines().map { it.toCharArray() }

        val emptyRows = lines.mapIndexedNotNull { index, row ->
            if (row.all { it == '.' }) index
            else null
        }

        val emptyColumns = lines[0].indices.mapIndexedNotNull { index, column ->
            if (lines.all { it[index] == '.' }) index
            else null
        }

        return lines.flatMapIndexed { y, row ->
            row.mapIndexed { x, cell ->
                if (cell == '#') Point(x + (emptyColumns.count { it < x } * expansionAddition),
                    y + (emptyRows.count { it < y } * expansionAddition))
                else null
            }.filterNotNull()
        }
    }
}