package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day9 : Day(2023, 9) {

    override fun part1(input: String) = parseInput(input)
        .map { sequence ->
            listOf(sequence) + findSubSequences(sequence)
        }
        .sumOf { subSequences ->
            subSequences
                .map(List<Int>::last)
                .sum()
        }

    private fun findSubSequences(sequence: List<Int>): List<List<Int>> {
        val nextSequence = findDifferences(sequence)

        return if (nextSequence.all { it == 0 }) listOf(nextSequence)
        else listOf(nextSequence) + findSubSequences(nextSequence)
    }

    private fun findDifferences(sequence: List<Int>): List<Int> =
        sequence
            .windowed(2, 1)
            .map { it[1] - it[0] }

    override fun part2(input: String) = parseInput(input)
        .map { sequence ->
            listOf(sequence) + findSubSequences(sequence)
        }
        .sumOf { subSequences ->
            subSequences
                .map(List<Int>::first)
                .reversed()
                .fold(0 as Int) { acc, i -> i - acc}
        }

    private fun parseInput(input: String) = input.lines()
        .map { line ->
            line
                .split(" ")
                .map(String::toInt)
        }
}