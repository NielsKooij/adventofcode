package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day1 : Day(2023, 1) {

    private val stringNumbers = listOf("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine")

    override fun part1(input: String) = input.lines()
        .map { line -> line.filter(Char::isDigit) }
        .map { "${it.first()}${it.last()}" }
        .map(String::toInt)
        .sum()

    override fun part2(input: String) = input.lines()
        .map(::normalize)
        .map { "${it.first()}${it.last()}" }
        .map(String::toInt)
        .sum()

    private fun normalize(line: String): String {
        var normalized = ""

        for(i in line.indices)
        {
            if(line[i].isDigit())
            {
                normalized += line[i]
            }

            stringNumbers.forEachIndexed { index, n ->
                val subString = line.substring(i)
                if(subString.startsWith(n))
                {
                    normalized += index.toString()
                }
            }
        }

        return normalized
    }
}