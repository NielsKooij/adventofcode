package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day14: Day(2023, 14) {

    override fun part1(input: String) = parseInput(input)
        .map(::calculateWeight)
        .sum()

    private fun calculateWeight(line: String) =
        line.foldIndexed(Pair(0, 0)) { index, acc, symbol ->
            when(symbol)
            {
                'O' -> Pair(acc.first + calculateWeight(acc.second, line.length), acc.second + 1)
                '#' -> Pair(acc.first, index + 1)
                else -> acc
            }
        }.first

    private fun calculateWeight(index: Int, lineWidth: Int) =
        lineWidth - index

    override fun part2(input: String) =
        "Not yet implemented"

    private fun parseInput(input: String): List<String> =
        input.lines()
            .let { lines ->
                val lineLength = lines[0].length

                (0 until lineLength).map { columnNumber ->
                    lines.map { it[columnNumber] }.joinToString("")
                }
            }
}

