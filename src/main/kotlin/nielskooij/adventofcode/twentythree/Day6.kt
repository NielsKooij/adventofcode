package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day
import nielskooij.adventofcode.util.product
import kotlin.math.pow
import kotlin.math.round
import kotlin.math.sqrt

class Day6: Day(2023, 6) {

    override fun part1(input: String) = parseInput(input)
        .map { race ->
            nrOfWaysToWinOptimized(race.first, race.second)
        }
        .product()

    private fun nrOfWaysToWin(time: Long, distanceToBeat: Long) =
        (0..time)
            .map { secondsOfWinding -> secondsOfWinding * (time - secondsOfWinding) }
            .count { it > distanceToBeat }

    override fun part2(input: String) = parseInputPart2(input)
        .let { race ->  nrOfWaysToWinOptimized(race.first, race.second) }

    private fun parseInput(input: String): List<Pair<Long, Long>> = input
        .split("\n")
        .asSequence()
        .map { it.replace("Time:", "") }
        .map { it.replace("Distance:", "") }
        .map { it.replace("\r", "") }
        .map { it.split(" ") }
        .map { number -> number.filter { it.isNotBlank() } }
        .map { it.map(String::toLong) }
        .toList()
        .let { it[0].zip(it[1]) }

    private fun parseInputPart2(input: String): Pair<Long, Long> = input
        .split("\n")
        .asSequence()
        .map { it.replace("Time:", "") }
        .map { it.replace("Distance:", "") }
        .map { it.replace("\r", "") }
        .map { it.replace(" ", "") }
        .map(String::toLong)
        .toList()
        .let { Pair(it[0], it[1]) }

    private fun nrOfWaysToWinOptimized(time: Long, distanceToBeat: Long): Long {
        val x1 = ((- (time) + sqrt(time.toDouble().pow(2.0) - 4 * distanceToBeat)) / -2)
        val x2 = ((- (time) - sqrt(time.toDouble().pow(2.0) - 4 * distanceToBeat)) / -2)

        return (round(x2) - round(x1) + 1).toLong()
    }
}