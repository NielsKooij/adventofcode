package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day2: Day(2023, 2) {

    private val predicateAmounts = mapOf(
        "red" to 12,
        "green" to 13,
        "blue" to 14
    )

    override fun part1(input: String) = parseInput(input)
        .filter { game -> game.rounds.all(::isPossibleRound) }
        .map(Game::gameNumber)
        .sum()

    private fun isPossibleRound(round: Round) =
        predicateAmounts.none { (colour, amount) ->
            round.getOrDefault(colour, 0) > amount
        }

    override fun part2(input: String) = parseInput(input)
        .map(Game::rounds)
        .map { rounds ->
            predicateAmounts.keys.map { key ->
                findMinimumAmountOfCubes(key, rounds)
            }
        }.sumOf(::multiplyList)

    private fun multiplyList(list: List<Int>): Int =
        list.fold(1) { acc, element -> acc * element }

    private fun findMinimumAmountOfCubes(cubeColour: String, rounds: List<Round>): Int =
        rounds.maxByOrNull { round -> round[cubeColour] ?: 1 }
            ?.get(cubeColour)
            ?: 1

    private fun parseInput(input: String): List<Game> = input.lines()
        .map {
            val parts = it.split(":")
            val gameNumber = parts[0]
                .replace("Game ", "")
                .toInt()

            val rounds = parts[1]
                .split(";")
                .map(::parseRound)

            Game(gameNumber, rounds)
        }

    private fun parseRound(input: String): Round = input
        .split(",")
        .associate {
            val parts = it.trim().split(" ")
            parts[1] to parts[0].toInt()
        }
}

data class Game(
    val gameNumber: Int,
    val rounds: List<Round>
)

typealias Round = Map<String, Int>