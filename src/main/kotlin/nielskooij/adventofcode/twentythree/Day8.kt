package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day
import java.lang.IllegalStateException

class Day8: Day(2023, 8) {

    override fun part1(input: String) = parseInput(input).let { puzzle ->
        val instructions = puzzle.instructions

        var counter = 0
        var currentStep = "AAA"
        while(currentStep != "ZZZ")
        {
            val instruction = instructions.get(counter % instructions.size)

            currentStep = when(instruction)
            {
                'L' -> puzzle.nodes[currentStep]!!.first
                'R' -> puzzle.nodes[currentStep]!!.second
                else -> throw IllegalStateException()
            }
            counter++
        }

        counter
    }

    override fun part2(input: String) =
        "Not yet implemented"

    private fun parseInput(input: String): PuzzleInput {
        val parts = input.split(System.lineSeparator() + System.lineSeparator())
        val instructions = parts[0].toCharArray().toList()

        val nodes = parts[1]
            .split(System.lineSeparator())
            .associate { line ->
                val lineParts = line.split(" = ")

                val pair = lineParts[1]
                    .replace("(", "")
                    .replace(")", "")
                    .split(", ")

                lineParts[0] to Pair(pair[0], pair[1])
            }

        return PuzzleInput(instructions, nodes)
    }
}

data class PuzzleInput(
    val instructions: List<Char>,
    val nodes: Map<String, Pair<String, String>>
)