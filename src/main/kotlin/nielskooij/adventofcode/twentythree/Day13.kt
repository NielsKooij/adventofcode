package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day13 : Day(2023, 13) {

    override fun part1(input: String) = input
        .split("\n\n")
        .sumOf { note ->
            findReflection(note.lines(), 0)
                ?.let { 100 * it }
                ?: findReflection(transpose(note.lines()), 0)
                ?: 0
        }

    override fun part2(input: String) = input
        .split("\n\n")
        .sumOf { note ->
            findReflection(note.lines(), 1)
                ?.let { 100 * it }
                ?: findReflection(transpose(note.lines()), 1)
                ?: 0
        }

    private fun findReflection(note: List<String>, diffs: Int): Int? {
        (1 until note.size).forEach { nr ->
            val mirrorRange = findMirrorRange(nr, note.size)
            val possibleMirror = note.drop(mirrorRange.first).take(mirrorRange.last - mirrorRange.first)
            val firstHalf = possibleMirror.take(possibleMirror.size / 2)
            val secondHalf = possibleMirror.drop(possibleMirror.size / 2)
            if (isMirrored(diffs, firstHalf, secondHalf)) return nr
        }
        return null
    }

    private fun findMirrorRange(nr: Int, maxLength: Int) =
        0.coerceAtLeast(nr - (maxLength - nr))..(maxLength).coerceAtMost(nr * 2)

    private fun isMirrored(allowedDiffs: Int, firstHalf: List<String>, secondHalf: List<String>): Boolean {
        val firstChars = firstHalf.map(String::toCharArray)
        val secondChars = secondHalf.map(String::toCharArray).reversed()

        val diffs = firstChars
            .zip(secondChars)
            .flatMap { (first, second) -> first.zip(second) }
            .filter { (first, second) -> first != second }
            .size

        return diffs == allowedDiffs
    }

    private fun transpose(note: List<String>) =
        note.map(String::toCharArray)
            .let { matrix ->
                (matrix[0].indices).map { joinNthElement(matrix, it) }
            }

    private fun joinNthElement(matrix: List<CharArray>, n: Int) =
        matrix.map { chars -> chars[n] }.joinToString("")
}
