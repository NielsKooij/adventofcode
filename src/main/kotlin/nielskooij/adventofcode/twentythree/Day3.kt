package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day3: Day(2023, 3) {

    override fun part1(input: String) = parseInput(input).let { elements ->
        val symbols = elements.filterIsInstance<Symbol>()

        elements
            .filterIsInstance<Number>()
            .filter { number -> hasAdjecentSymbol(number, symbols) }
            .sumOf(Number::value)
    }

    private fun hasAdjecentSymbol(number: Number, symbols: List<Symbol>) =
        symbols.any { symbol -> isAdjecentTo(symbol, number) }


    override fun part2(input: String) = parseInput(input).let { elements ->
        val symbols = elements.filterIsInstance<Symbol>()
        val numbers = elements.filterIsInstance<Number>()

        symbols
            .filter { symbol -> symbol.symbol == '*' }
            .map { symbol ->
                numbers.filter { number -> isAdjecentTo(symbol, number) }
            }
            .filter { it.size == 2 }
            .sumOf { it[0].value * it[1].value }
    }

    private fun isAdjecentTo(symbol: Symbol, number: Number) =
        symbol.rowNumber in number.rowRange && symbol.columnNumber in number.columnRange

    private fun parseInput(input: String): List<Element>  = input.lines()
        .flatMapIndexed { rowNumber, it ->
            val elements = mutableListOf<Element>()

            var line = it
            var index = 0
            while(line.isNotEmpty())
            {

                val numbers = line.takeWhile(Char::isDigit)
                if(numbers.isNotBlank())
                {
                    val number = Number(
                        numbers.toInt(),
                        0.coerceAtLeast(rowNumber - 1) .. (rowNumber + 1),
                        0.coerceAtLeast(index - 1) .. (index + numbers.length)
                    )
                    elements.add(number)
                    index += numbers.length
                    line = line.substring(numbers.length)
                }

                val dots = line.takeWhile { char -> char == '.' }
                if(dots.isNotBlank())
                {
                    index += dots.length
                    line = line.substring(dots.length)
                }

                line
                    .takeIf(String::isNotEmpty)
                    ?.get(0)
                    ?.takeIf { char -> char != '.' && !char.isDigit() }
                    ?.let {
                        elements.add(Symbol(it, rowNumber, index))
                        index++
                        line = line.substring(1)
                    }
            }

            elements.toList()
        }
}

sealed class Element

data class Number(
    val value: Int,
    val rowRange: IntRange,
    val columnRange: IntRange
): Element()

data class Symbol(
    val symbol: Char,
    val rowNumber: Int,
    val columnNumber: Int
): Element()