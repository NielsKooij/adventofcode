package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day12: Day(2023, 12) {

    override fun part1(input: String) = parseInput(input)
        .flatMap { calculatePossibilities(it.first, it.second) }
        .count()

    override fun part2(input: String) =
        "Not yet implemented"

    private fun calculatePossibilities(brokenData: String, listNotation: List<Int>): List<String> {
        if(brokenData.none { it == '?' }) {
            return if(isValid(brokenData, listNotation)) listOf(brokenData) else emptyList()
        }

        return listOf(
            brokenData.replaceFirst("?", "."),
            brokenData.replaceFirst("?", "#")
        )
            .filter { isPartiallyValid(it, listNotation) }
            .flatMap { calculatePossibilities(it, listNotation) }
    }

    private fun isPartiallyValid(brokenData: String, listNotation: List<Int>) = brokenData
        .substring(0, brokenData.indexOfFirst { it == '?' }.takeIf { it > 0 } ?: brokenData.length)
        .split(".")
        .filter(String::isNotEmpty)
        .map { it.length }
        .dropLast(1)
        .zip(listNotation)
        .all { zipped -> zipped.first == zipped.second }

    private fun isValid(brokenData: String, listNotation: List<Int>) = brokenData
        .split(".")
        .filter(String::isNotEmpty)
        .map { it.length }
        .let {
            it.size == listNotation.size && it.zip(listNotation).all { zipped -> zipped.first == zipped.second }
        }

    private fun parseInput(input: String) = input.lines()
        .map { line ->
            val parts = line.split(" ")
            val listData = parts[1].split(",").map(String::toInt)
            Pair(parts[0], listData)
        }
}