package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day

class Day15: Day(2023, 15) {

    override fun part1(input: String) = input
        .split(",")
        .sumOf(::calculateHash)

    private fun calculateHash(str: String): Int =
        str.fold(0) { acc, c ->
            ((acc + c.toInt()) * 17) % 256
        }

    override fun part2(input: String) = input.split(",")
        .fold(initializeLens()) { acc, instruction -> executeInstruction(acc, instruction) }
        .mapIndexed { index, lensBox ->
            lensBox.foldIndexed(0) { boxSlot, boxAcc, lens ->
                boxAcc + ((index + 1) * (boxSlot + 1) * lens.focalLength)
            }
        }
        .sum()

    private fun executeInstruction(lenses: MutableList<List<Lens>>, instruction: String): MutableList<List<Lens>> {
        val label = instruction.takeWhile { it.isLetter() }
        val hash = calculateHash(label)
        if (instruction.contains("-"))
        {
            lenses[hash] = lenses[hash].filter { it.label != label }
        }
        else
        {
            val focalLength = instruction.dropWhile { !it.isDigit() }.toInt()
            val lens = Lens(label, focalLength)

            val index = lenses[hash].indexOfFirst { it.label == label }
            if(index == -1) lenses[hash] = lenses[hash] + listOf(lens)
            else {
                lenses[hash] = replaceLens(lenses[hash], index, lens)
            }
        }

        return lenses
    }

    private fun replaceLens(lenses: List<Lens>, index: Int, newLens: Lens) =
        lenses.filter { it.label != newLens.label }.toMutableList().also {
            it.add(index, newLens)
        }

    private fun initializeLens(): MutableList<List<Lens>> = mutableListOf<List<Lens>>().let { lens ->
        (0 until 256).forEach { _ ->
            lens.add(listOf())
        }
        lens
    }
}

data class Lens(val label: String, val focalLength: Int)