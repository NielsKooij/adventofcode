package nielskooij.adventofcode.twentythree

import nielskooij.adventofcode.twentyone.Day
import nielskooij.adventofcode.util.find

class Day5 : Day(2023, 5) {

    override fun part1(input: String) = parseInput(input, parseSeeds()).let { puzzle ->
        puzzle.seeds
            .minOfOrNull { seed -> findSeedLocation(seed.first, puzzle.maps) }
            ?: throw IllegalStateException("")
    }

    private fun findSeedLocation(seed: Long, mappings: List<Map<LongRange, LongRange>>) = mappings
        .fold(seed) { acc, map ->
            map
                .find { acc in it }
                ?.let { (acc - it.key.first) + it.value.first }
                ?: acc
        }

    override fun part2(input: String) = parseInput(input, parseSeedRanges()).let { puzzle ->
        puzzle.maps
            .fold(puzzle.seeds) { seeds, map ->
                seeds.flatMap { seedRange ->
                    mapLocations(seedRange, map)
                }
            }
            .minOf { it.first }
    }

    private fun mapLocations(seeds: LongRange, map: Map<LongRange, LongRange>): List<LongRange> {
        val mapping = map.find { key -> seeds.first in key }
        return if (mapping != null) {
            if (seeds.last in mapping.key) listOf(map(seeds, mapping))
            else listOf(map(seeds.first..mapping.key.last, mapping)) + mapLocations(
                mapping.key.last + 1..seeds.last,
                map
            )
        } else {
            val nextMap = map.find { key -> key.first > seeds.first && key.first <= seeds.last }
            if (nextMap == null) listOf(seeds)
            else listOf(seeds.first until nextMap.key.first) + mapLocations(nextMap.key.first..seeds.last, map)
        }
    }

    private fun map(seeds: LongRange, map: Map.Entry<LongRange, LongRange>): LongRange {
        val diff = seeds.first - map.key.first
        val seedsLength = seeds.last - seeds.first
        return map.value.first + diff..map.value.first + diff + seedsLength
    }

    private fun parseSeedRanges() = { seeds: String ->
        seeds
            .replace("seeds: ", "")
            .split(" ")
            .map(String::toLong)
            .windowed(2, 2)
            .map { it.first() until (it.first() + it.last()) }
    }

    private fun parseSeeds() = { seeds: String ->
        seeds
            .replace("seeds: ", "")
            .split(" ")
            .map(String::toLong)
            .map { it until it + 1 }
    }

    private fun parseInput(input: String, seedParser: (String) -> List<LongRange>) = input
        .split(System.lineSeparator() + System.lineSeparator())
        .let { parts ->
            PuzzleInputDay5(
                seedParser.invoke(parts[0]),
                parseMaps(parts.drop(1))
            )
        }

    private fun parseMaps(maps: List<String>) =
        maps.map { map ->
            map
                .split(System.lineSeparator())
                .drop(1)
                .associate { mapping ->
                    val mappingParts = mapping.split(" ").map(String::toLong)
                    val destinationRange = mappingParts[0] until mappingParts[0] + mappingParts[2]
                    val sourceRange = mappingParts[1] until mappingParts[1] + mappingParts[2]
                    sourceRange to destinationRange
                }
                .toSortedMap { k1, k2 -> k1.first.compareTo(k2.first) }
        }
}

data class PuzzleInputDay5(
    val seeds: List<LongRange>,
    val maps: List<Map<LongRange, LongRange>>
)