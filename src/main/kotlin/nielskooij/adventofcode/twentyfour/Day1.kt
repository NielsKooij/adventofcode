package nielskooij.adventofcode.twentyfour

import nielskooij.adventofcode.twentyone.Day
import kotlin.math.abs

class Day1 : Day(2024, 1) {

    override fun part1(input: String): Any = parse(input)
        .let { it.first.sorted().zip(it.second.sorted()) }
        .map { it.first - it.second }
        .map(::abs)
        .sum()

    override fun part2(input: String): Any = parse(input)
        .let { it.first.map { elementFirstList -> elementFirstList * it.second.count { elementSecondList -> elementFirstList == elementSecondList } } }
        .sum()

    private fun parse(input: String) =
        input.lines()
            .map { it.split(" ").filter { it.isNotBlank() } }
            .map { Pair(it[0].toInt(), it[1].toInt()) }
            .unzip()
}