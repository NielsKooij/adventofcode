package nielskooij.adventofcode.twentyfour

import nielskooij.adventofcode.twentyone.Day

class Day2: Day(2024, 2) {

    override fun part1(input: String): Any =
        input.lines()
            .map { it.split(" ").map(String::toInt) }
            .map(::isSafe)
            .count { it }

    private fun isSafe(levels: List<Int>): Boolean =
        levels.windowed(2, 1).let { level ->
            level.all { (it[0] > it[1] && it[0] - it[1] <= 3) } || level.all { it[0] < it[1] && it[1] - it[0] <= 3 }
        }

    override fun part2(input: String): Any {
        TODO("Not yet implemented")
    }
}