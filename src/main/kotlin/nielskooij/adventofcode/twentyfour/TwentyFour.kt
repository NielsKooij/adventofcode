package nielskooij.adventofcode.twentyfour

fun main() {
    Day1().run()
    Day2().run()
}