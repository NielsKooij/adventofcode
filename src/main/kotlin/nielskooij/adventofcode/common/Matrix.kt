package nielskooij.adventofcode.common

class Matrix<T>(val cells: List<T>, val m: Int, val n: Int) {

    fun getAdjacentCells(dim: Pair<Int, Int>): List<T> =
        getAdjacentCells(dim) { _, _ ->
            val x = dim.first
            val y = dim.second
            listOf(
                Pair(x - 1, y - 1),
                Pair(x - 1, y),
                Pair(x - 1, y + 1),
                Pair(x, y - 1),
                Pair(x, y + 1),
                Pair(x + 1, y - 1),
                Pair(x + 1, y),
                Pair(x + 1, y + 1)
            )
        }

    fun getAdjacentCells(dim: Pair<Int, Int>, f: (Matrix<T>, Pair<Int, Int>) -> List<Pair<Int, Int>>): List<T> =
        f.invoke(this, dim).filter {
            it.first in 0 until m && it.second in 0 until n
        }.mapNotNull {
            dimToIndex(it)?.let { index -> cells[index] }
        }

    fun <S> map(f: (Matrix<T>, T) -> S): Matrix<S> =
        Matrix(cells.map { c -> f.invoke(this, c) }, m, n)

    fun dimToIndex(dim: Pair<Int, Int>): Int? =
        ((n * dim.first) + (dim.second % m)).takeIf { it in cells.indices }

    fun indexToDim(index: Int): Pair<Int, Int> =
        Pair(index / n, index % n)

    fun get(dim: Pair<Int, Int>): T? =
        dimToIndex(dim)?.let { cells[it] }
}