package nielskooij.adventofcode.common

import java.io.File

object ResourceHelper
{
    fun getResource(relativePath: String): List<String> =
        File(getResourcePath(relativePath)).useLines {
            return it.toList()
        }

    fun getResourceAsIntList(relativePath: String): List<Int> =
        getResource(relativePath).map { it.toInt() }

    fun getResourceAsString(relativePath: String): String =
        File(getResourcePath(relativePath)).readText()

    private fun getResourcePath(relativePath: String): String =
        ResourceHelper::class.java.getResource(relativePath).path
}
