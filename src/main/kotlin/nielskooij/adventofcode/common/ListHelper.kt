package nielskooij.adventofcode.common

inline fun <T, U, reified V> List<T>.zipWith(other: List<U>, f: (T, U) -> V): List<V> =
    this.zip(other).map { (t, u) -> f.invoke(t, u) }

inline fun <T> List<T>.split(predicate: (T) -> Boolean): List<List<T>> = this
    .fold(Pair(mutableListOf<List<T>>(), mutableListOf<T>())) { acc, i ->
        if(predicate.invoke(i))
        {
            acc.first.add(acc.second)
            Pair(acc.first, mutableListOf())
        }
        else
        {
            acc.second.add(i)
            acc
        }
    }
    .let { (acc, remainder) ->
        if(remainder.size > 0) acc.add(remainder)
        acc
    }

inline fun <T> List<T>.sumBy(f: (T) -> Long): Long =
    this.fold(0) { acc, t ->
        acc + f.invoke(t)
    }
