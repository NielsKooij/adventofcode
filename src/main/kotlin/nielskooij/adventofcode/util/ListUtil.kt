package nielskooij.adventofcode.util

fun List<Long>.product() =
    this.fold(1L) { acc, i -> acc * i }