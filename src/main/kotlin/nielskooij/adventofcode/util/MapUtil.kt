package nielskooij.adventofcode.util

fun <K, V> Map<K, V>.find(predicate: (K) -> Boolean): Map.Entry<K, V>? =
    this.entries.find { predicate.invoke(it.key) }