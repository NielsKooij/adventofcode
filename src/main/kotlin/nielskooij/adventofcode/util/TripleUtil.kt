package nielskooij.adventofcode.util

fun Triple<Int, Int, Int>.add(other: Triple<Int, Int, Int>): Triple<Int, Int, Int> =
    Triple(this.first + other.first, this.second + other.second, this.third + other.third)

fun Triple<Int, Int, Int>.scale(scalar: Int): Triple<Int, Int, Int> =
    Triple(first * scalar, second * scalar, third * scalar)
