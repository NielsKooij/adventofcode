package nielskooij.adventofcode.util

fun Pair<Int, Int>.add(other: Pair<Int, Int>): Pair<Int, Int> =
    Pair(this.first + other.first, this.second + other.second)

fun Pair<Int, Int>.scale(scalar: Int): Pair<Int, Int> =
    Pair(first * scalar, second * scalar)
